const { app, BrowserWindow, globalShortcut } = require('electron')

function createWindow() {
    const win = new BrowserWindow({
        width: 500,
        height: 330,
        webPreferences: {
            nodeIntegration: true
        },
        frame: false,
        resizable: false,
        maximazable: false,
        fullscreenable: false,
    })

    win.loadFile('./public/index.html')
    win.fullScreenable = false;
    win.maximizable = false;
}

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow()
    }
})
