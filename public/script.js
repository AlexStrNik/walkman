const { remote, ipcRenderer } = require('electron');
let howler = require('howler');
let dnd = document.getElementById('drag-area');
let jsmediatags = require("jsmediatags");

howler.Howler.usingWebAudio = false;

let speed = 1;
let angle = 0;
let animation = undefined;

const startAnimation = () => {
    if (animation != undefined) {
        return
    }

    requestAnimationFrame(animate)
}

const stopAnimation = () => {
    if (animation != undefined) {
        cancelAnimationFrame(animation)
        animation = undefined;
    }
}

const setTitle = (title) => {
    document.getElementById("title").textContent = title;

    if (navigator.mediaSession.metadata == undefined) {
        navigator.mediaSession.metadata = new MediaMetadata({});
    }

    navigator.mediaSession.metadata.title = title;
}

const setCover = (cover) => {
    if (navigator.mediaSession.metadata == undefined) {
        navigator.mediaSession.metadata = new MediaMetadata({});
    }

    navigator.mediaSession.metadata.artwork = cover;
}

const setMetas = (artist, album) => {
    if (navigator.mediaSession.metadata == undefined) {
        navigator.mediaSession.metadata = new MediaMetadata({});
    }

    navigator.mediaSession.metadata.artist = artist;
    navigator.mediaSession.metadata.album = album;
}

const onPlay = (track) => {
    navigator.mediaSession.playbackState = "playing";
    startAnimation();

    document.getElementById('play').src = "./icons/pause.svg"

    track.volume(1.0);

    track.metaUpdated = (track) => {
        setTitle(track.title);
        setCover(track.cover);
        setMetas(track.artist, track.album);

        track.metaUpdated = undefined;
    }

    if (track.title != undefined) {
        setTitle(track.title);
    } else {
        setTitle("Unknown");
    }

    setCover(track.cover);
    setMetas(track.artist, track.album);
}

const onPause = (track) => {
    navigator.mediaSession.playbackState = "paused";
    stopAnimation();

    document.getElementById('play').src = "./icons/play.svg";
}

const onEnd = (track) => {
    navigator.mediaSession.playbackState = "none";
    stopAnimation();
    document.getElementById('play').src = "./icons/play.svg";

    track.metaUpdated = undefined;

    if (current == undefined) {
        return
    }

    current += 1;

    if (playlist.length > current) {
        playlist[current].play()
    } else {
        document.getElementById("title").textContent = "Drag audio file to play!";
    }
}

const onFade = (track, id) => {
    track.howl.stop(id);
    speed = 1;
}

class Track {
    constructor(file) {
        this.id = undefined;
        this.howl = new Howl({
            src: file,
            html5: true,
            onpause: () => onPause(this),
            onplay: () => onPlay(this),
            onend: () => onEnd(this),
            onseek: () => speed = 1,
        })

        let self = this;

        new jsmediatags.Reader(file)
            .setTagsToRead(['title', 'picture', 'artist', 'album'])
            .read({
                onSuccess: (tag) => {
                    self.title = tag.tags.title;
                    self.album = tag.tags.album;
                    self.artist = tag.tags.artist;

                    let cover = tag.tags.picture;
                    let buffer = Buffer.from(cover.data);

                    self.cover = [{ src: `data:${cover.format};base64,${buffer.toString('base64')}`, sizes: '512x512', type: cover.format }]

                    if (self.metaCb != undefined) {
                        self.metaCb(self)
                    }
                },
                onError: (error) => {
                    console.log(':(', error.type, error.info);
                }
            });
    }

    play() {
        this.id = this.howl.play()
    }

    stop() {
        this.howl.stop(this.id);
    }

    pause() {
        this.howl.pause(this.id)
    }

    playing() {
        return this.howl.playing(this.id)
    }

    state() {
        return this.howl.state()
    }

    seek(value) {
        return this.howl.seek(value, this.id)
    }

    fade(from, to, duration) {
        let self = this
        let id = self.id

        this.howl.once('fade', () => onFade(self, id))
        this.howl.fade(from, to, duration, this.id)
    }

    volume(level) {
        this.howl.volume(level, this.id)
    }

    metaUpdated(cb) {
        this.metaCb = cb;
    }
}

const animate = () => {
    document.documentElement.style.setProperty('--rotation', angle + 'deg')
    angle = (angle + speed) % 360;
    animation = requestAnimationFrame(animate);
}

let playlist = [];
let current = undefined;

dnd.ondragover = () => {
    return false;
};

dnd.ondragleave = () => {
    return false;
};

dnd.ondragend = () => {
    return false;
};

dnd.ondrop = (e) => {
    e.preventDefault();

    Array.from(e.dataTransfer.files).forEach((file) => {
        playlist.push(new Track(file.path))
    })

    if (current == undefined) {
        playlist[0].play();
        current = 0;
    }
};

const playpause = () => {
    if (playlist.length == 0) {
        return;
    }

    if (current == undefined || current == playlist.length) {
        current = 0
    }

    if (playlist[current].state() != 'loaded') {
        return;
    }

    if (playlist[current].playing()) {
        playlist[current].pause();
    } else {
        playlist[current].play();
    }
}

const next = () => {
    if (playlist.length == 0) {
        return;
    }

    if (current == undefined || current == playlist.length) {
        current = 0;
    }

    speed = 6;

    let prev = current;
    current = (current + 1) % playlist.length;

    playlist[prev].fade(1.0, 0.0, 500);
    playlist[current].play();
}

const prev = () => {
    if (playlist.length == 0) {
        return;
    }

    if (current == undefined) {
        current = 1;
    }

    speed = -6;

    if (playlist[current].seek() >= 4.0) {
        setTimeout(() => {
            playlist[current].seek(0);
        }, 500);
    } else {
        let prev = current;
        current = (current + playlist.length - 1) % playlist.length;

        playlist[prev].fade(1.0, 0.0, 500);
        playlist[current].play();
    }
}

document.getElementById('play').onclick = () => {
    playpause();
}

document.getElementById('next').onclick = () => {
    next();
}

document.getElementById('prev').onclick = () => {
    prev();
}

navigator.mediaSession.setActionHandler('play', playpause);
navigator.mediaSession.setActionHandler('pause', playpause);
navigator.mediaSession.setActionHandler('previoustrack', prev);
navigator.mediaSession.setActionHandler('nexttrack', next);

document.getElementById('close').onclick = () => {
    let window = remote.getCurrentWindow();
    window.close();
}